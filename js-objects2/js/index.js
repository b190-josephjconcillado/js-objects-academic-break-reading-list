console.log("Hello World!");

function Character(name,characterClass,level){
    this.name= name;
    this.characterClass= characterClass;
    this.level = level;
    if(this.characterClass.toLowerCase() == "dark knight"){
        this.hp = 100*level;
        this.attack = 100*level;
        this.skill = ["Falling Slash","Cyclone Slash","Death Stab"];
        
    }else if(this.characterClass.toLowerCase() == "dark wizard"){
        this.hp = 80*level;
        this.attack = 120*level;
        this.skill = ["Fire Ball","Meteor","Aquabeam"];
    }else if(this.characterClass.toLowerCase() == "fairy elf"){
        this.hp = 90*level;
        this.attack = 110*level;
        this.skill = ["Ice Arrow","Triple Shot","Summon Soldier"];
    }else{
        console.log("Unknown Character Class");
    }
};

let IAmDk = new Character("IamDK","Dark Knight",100);
let IAmDw = new Character("IamDW","Dark Wizard",100);
let IAmElf = new Character("IamElf","Fairy Elf",100);

console.log(IAmDk.characterClass);
console.log(IAmDk);
console.log(IAmDw.characterClass);
console.log(IAmDw);
console.log(IAmElf.characterClass);
console.log(IAmElf);