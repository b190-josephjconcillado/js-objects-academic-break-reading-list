console.log("Hello World!");

function BookDetails(author,title,publishedDate,summary){
    this.author= author;
    this.title= title;
    this.publishedDate= publishedDate;
    this.summary= summary;
};

let boyMeetsGirl = new BookDetails("Joshua Harris", "Boy Meets Girl", "July 26, 2005", `The last thing singles want is more rules. But if you're looking for an intentional, God-pleasing game plan for finding a future spouse, Joshua Harris delivers an appealing one. A compelling new foreword, an all-new "8 Great Courtship Conversations" section, and updated material throughout makes this five-year revision of the original Boy Meets Girl a must-have! Harris illustrates how biblical courtship--a healthy, joyous alternative to recreational dating--worked for him and his wife. Boy Meets Girl presents an inspiring, practical example for readers wanting to pursue the possibility of marriage with someone they may be serious about.`);
let relationship101 = new BookDetails("John C. Maxwell","Relationship 101", "January 13, 2004", `Great leaders understand that their team is only as strong as its weakest link. And healthy, nurtured relationships are key in making your team operate as a well-oiled machine. In Relationships 101, John Maxwell offers time-tested principles for connecting with people and building positive working relationships within an organization. The unique, pocket-size format puts straightforward, practical information at your fingertips. Topics include:
What Do I Need to Know About Others?
How Can I Become a Better Listener?
What Does It Mean to Have Integrity with People?
How Can I Serve and Lead at the Same Time?`)
let doHardThings = new BookDetails("Alex Harris","Do Hard Things", "April 15, 2008", `A growing movement of young people is rebelling against the low expectations of today's culture by choosing to "do hard things" for the glory of God. And Alex and Brett Harris are leading the charge. Do Hard Things is the Harris twins' revolutionary message in its purest and most compelling form, giving readers a tangible glimpse of what is possible for teens who actively resist cultural lies that limit their potential.`)
console.log(`Book title: ${boyMeetsGirl.title}.`);
console.log(boyMeetsGirl);
console.log(`Book title: ${relationship101.title}.`);
console.log(relationship101);
console.log(`Book title: ${doHardThings.title}.`);
console.log(doHardThings);